//
//  SettingRouter.swift
//  Github-MVVM-Bound
//
//  Created by Nghia Tran on 3/20/17.
//  Copyright © 2017 nghiatran. All rights reserved.
//

import Foundation

class ProfileRouter: Router {
    
    typealias Element = ProfileViewController
    
    var routerType: RouterType {
        return .push
    }
    
    fileprivate lazy var _viewController: ProfileViewController = {
        return ProfileViewController.xibView()
    }()
    
    var viewController: ProfileViewController {
        return self._viewController
    }
}
