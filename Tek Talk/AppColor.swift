//
//  AppColor.swift
//  YoSing-iOS
//
//  Created by Hiep Nguyen on 4/14/16.
//  Copyright © 2016 Zyncas. All rights reserved.
//

import UIKit

struct AppColor {
    static let DEFAULT_COLOR		: Int = 0x4990E2
    static let LIGHT_GRAY_COLOR     : Int = 0xE9E9E9
    static let DARK_GRAY_COLOR      : Int = 0x4A4A4A
    
}

class Colors : UIView {
    let colorTop = UIColor.clear.cgColor
    let colorBottom = UIColor.color(hex: 0x000000, alpha: 0.3).cgColor
    var gl: CAGradientLayer!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        gl = CAGradientLayer()
        gl.colors = [colorTop, colorBottom]
        gl.locations = [0.0, 1.0]
        self.gl.frame = self.bounds
        self.layer.addSublayer(self.gl)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.gl.frame = self.bounds
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
