//
//  CommonDataSourceDelegate.swift
//  Tek Talk
//
//  Created by Nghia Tran on 4/10/17.
//  Copyright © 2017 Zyncas Technologies. All rights reserved.
//

import UIKit

protocol CommonDataSourceDelegate: class {
    
    // Require
    func numberOfSection() -> Int
    func numberOfRow(in section: Int) -> Int
    func object(at indexPath: IndexPath) -> BaseObj
    
    // Option
    func heightForRow(at indexPath: IndexPath) -> CGFloat
}

//
// MARK: - Default extension
extension CommonDataSourceDelegate {
    
    func heightForRow(at indexPath: IndexPath) -> CGFloat {
        return 44.0
    }
}
