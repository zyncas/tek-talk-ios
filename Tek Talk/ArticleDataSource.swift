//
//  ArticleDataSource.swift
//  Tek Talk
//
//  Created by Nghia Tran on 4/10/17.
//  Copyright © 2017 Zyncas Technologies. All rights reserved.
//

import UIKit

class ArticleDataSource: BaseTableViewDataSource {

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ArticleCell.identifier, for: indexPath) as! ArticleCell
        let obj = self.delegate!.object(at: indexPath) as! ArticleObj
        
        cell.configureCellWithObj(obj)
        
        return cell
    }
}
