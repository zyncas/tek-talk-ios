//
//  Request.swift
//  Github-MVVM-Bound
//
//  Created by Nghia Tran on 3/20/17.
//  Copyright © 2017 nghiatran. All rights reserved.
//

import UIKit
import Alamofire
import RxSwift

//
// MARK: - Request protocol
protocol Requestable: URLRequestConvertible {
    
    associatedtype T
    
    var basePath: String {get}
    
    var endpoint: String {get}
    
    var httpMethod: HTTPMethod {get}
    
    var param: Parameters? {get}
    
    var addionalHeader: HeaderParameter? {get}
    
    var parameterEncoding: ParameterEncoding {get}
    
    func toAlamofire() -> Observable<T>
    
    func decode(data: Any) -> T?
}

//
// MARK: - Conform URLConvitible from Alamofire
extension Requestable {
    func asURLRequest() -> URLRequest {
        return self.buildURLRequest()
    }
}

//
// MARK: - Default implementation
extension Requestable {

    typealias HeaderParameter = [String: String]
    typealias JSONDictionary = [String: Any]
    
    var basePath: String {get {return Constants.App.Enviroment.API_DOMAIN}}
    var param: Parameters? {get {return nil}}
    var addionalHeader: HeaderParameter? {get {return nil}}
    var defaultHeader: HeaderParameter {get {return ["Accept": "application/json"]}}
    var urlPath: String {return basePath + endpoint}
    var url: URL {return URL(string: urlPath)!}
    var parameterEncoding: ParameterEncoding {get {return JSONEncoding.default}}
    
    func toAlamofire() -> Observable<T> {
        
        return Observable<T>.create { (observer) -> Disposable in
            
            guard let urlRequest = try? self.asURLRequest() else {
                observer.on(.error(NSError.unknowError()))
                return Disposables.create {}
            }
            
            Alamofire.request(urlRequest)
                .validate(statusCode: 200..<300)
                .validate(contentType: ["application/json"])
                .responseJSON(completionHandler: { (response) in
                    
                    // Check error
                    if let error = response.result.error {
                        observer.on(.error(error))
                        return
                    }
                    
                    // Check Response
                    guard let data = response.result.value else {
                        observer.on(.error(NSError.jsonMapperError()))
                        return
                    }
                    
                    // Parse here
                    guard let result = self.decode(data: data) else {
                        observer.on(.error(NSError.jsonMapperError()))
                        return
                    }
                    
                    // Fill
                    observer.on(.next(result))
                    observer.on(.completed)
                })
            
            return Disposables.create {
                
            }
        }
    }
    
    func buildURLRequest() -> URLRequest {
        
        // Init
        var urlRequest = URLRequest(url: self.url)
        urlRequest.httpMethod = self.httpMethod.rawValue
        urlRequest.timeoutInterval = TimeInterval(10 * 1000)
        
        // Encode param
        var request = try! self.parameterEncoding.encode(urlRequest, with: self.param)
        
        // Add addional Header if need
        if let additinalHeaders = self.addionalHeader {
            for (key, value) in additinalHeaders {
                request.addValue(value, forHTTPHeaderField: key)
            }
        }
        
        return request
    }
}
