//
//  NavigationItem.swift
//  Tek Talk
//
//  Created by Doyle Illusion on 1/5/17.
//  Copyright © 2017 Zyncas Technologies. All rights reserved.
//

import UIKit

class NavigationBar: UINavigationBar {
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        self.tintColor = UIColor.color(hex: 0x4A4A4A)
        self.backgroundColor = UIColor.white
        self.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.color(hex: 0x4A4A4A), NSFontAttributeName: UIFont(name: AppFontName.HEAVY, size: 16.0)!]
    }
}
