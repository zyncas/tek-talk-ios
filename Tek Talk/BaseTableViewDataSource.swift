//
//  BaseTableViewDataSource.swift
//  Tek Talk
//
//  Created by Nghia Tran on 4/10/17.
//  Copyright © 2017 Zyncas Technologies. All rights reserved.
//

import UIKit

class BaseTableViewDataSource: NSObject {

    //
    // MARK: - Variable
    fileprivate var tableView: UITableView!
    weak var delegate: CommonDataSourceDelegate?
    
    //
    // MARK: - Init
    init(with tableView: UITableView) {
        self.tableView = tableView
        super.init()
        self.initTableView()
    }
    
    //
    // MARK: - Public
    func registerCell<T>(_ cell: T.Type) where T: Identifier & XibInitialization {
        
        self.tableView.register(cell.xib(), forCellReuseIdentifier: cell.identifier)
    }
}

//
// MARK: - Private
extension BaseTableViewDataSource {
    
    fileprivate func initTableView() {
        self.tableView.dataSource = self
        self.tableView.delegate = self
    }
}

extension BaseTableViewDataSource: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        guard let delegate = self.delegate else {
            return 0
        }
        
        return delegate.numberOfSection()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let delegate = self.delegate else {
            return 0
        }
        
        return delegate.numberOfRow(in: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Need override
        return UITableViewCell()
    }
}
