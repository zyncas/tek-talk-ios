//
//  AppConfig.swift
//  YoSing-iOS
//
//  Created by Thanh on 5/9/16.
//  Copyright © 2016 Cao Phuoc Thanh. All rights reserved.
//

import UIKit

struct AppConfig {
    
    // MARK: - Enviroments
    
    struct Enviroment {
        
        /** API Domain */
        static var API_DOMAIN: String {
            set {}
            get {
                #if DEVELOPMENT
                    return "https://yosing.vn"
                #else
                    return "https://yosing.vn"
                #endif
            }
        }
        
        static var APP_VERSION: String {
            set {}
            get {
//                #if DEVELOPMENT
                    return "master"
//                #else
//                    return "v1.1"
//                #endif
            }
        }
        
    }
    
    // MARK: - Facebook
    
    struct Facebook {
        
        /** Facebook read permisstion **/
        static let FACEBOOK_LOGIN_READ_PERMISSTION: Array  = ["public_profile", "email", "user_birthday", "user_friends", "user_likes"]
        
        /** Facebook write permission **/
        static let FACEBOOK_LOGIN_WRITE_PERMISSION: Array = ["publish_actions"]
        
        /** Facebook login permisstion **/
        static let PUBLIC_PROFILE_PERMISSION    = ["fields": "id, name, email, cover, first_name, last_name, picture.width(200).height(200)"]
        
        /** Facebook app link **/
//        static let FACEBOOK_APP_LINK            = "https://fb.me/280493085622834"
    }
    
    // MARK: - Parse Push Services
    
    struct Device {
        // Main Screen
        static let SCREEN_HEIGHT = UIScreen.main.bounds.size.height
        static let SCREEN_WIDTH = UIScreen.main.bounds.size.width
        static let ORIGINAL_MAX_WIDTH = 640.0
        static let KEYWINDOW = UIApplication.shared.delegate!.window!
        
        static let IS_IPAD = (UI_USER_INTERFACE_IDIOM() == .pad)
        static let IS_IPHONE = (UI_USER_INTERFACE_IDIOM() == .phone)
        static let IS_RETINA = (UIScreen.main.scale >= 2.0)
        static let SCREEN_MAX_LENGTH = (max(SCREEN_WIDTH, SCREEN_HEIGHT))
        static let SCREEN_MIN_LENGTH = (min(SCREEN_WIDTH, SCREEN_HEIGHT))
        static let IS_IPHONE_4_OR_LESS = (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
        static let IS_IPHONE_5 = (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
        static let IS_IPHONE_6 = (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
        static let IS_IPHONE_6P = (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)
    }
}
