//
//  BaseTabBarController.swift
//  Tek Talk
//
//  Created by Nghia Tran on 4/10/17.
//  Copyright © 2017 Zyncas Technologies. All rights reserved.
//

import UIKit

enum TabbarType: Int {
    case question = 0,
    article,
    connect,
    talk,
    profile
}

class BaseTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.initCommon()
        self.initTabbarController()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

//
// MARK: - Private
extension BaseTabBarController {
    
    fileprivate func initCommon() {
        
    }
    
    fileprivate func initTabbarController() {
        
        // TODO: Need elegant solution here
        // Workaround to make it quick
        
        // Question 
        let questionVC = QuestionViewController.xibView()
        self.navigationController(at: .question).viewControllers = [questionVC]
        
        // Article
        let articleVC = ArticleViewController.xibView()
        self.navigationController(at: .article).viewControllers = [articleVC]
        
        // Connect
        let connectVC = ArticleViewController.xibView()
        self.navigationController(at: .connect).viewControllers = [connectVC]
        
        // Talk
        let talkVC = ArticleViewController.xibView()
        self.navigationController(at: .talk).viewControllers = [talkVC]
        
        // Profile
        let profileVC = ArticleViewController.xibView()
        self.navigationController(at: .profile).viewControllers = [profileVC]
        
    }
    
    fileprivate func navigationController(at type: TabbarType) -> UINavigationController {
        return self.viewControllers![type.rawValue] as! UINavigationController
    }
}
