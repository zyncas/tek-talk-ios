//
//  SearchRepoRoutes.swift
//  Github-MVVM-Bound
//
//  Created by Nghia Tran on 3/20/17.
//  Copyright © 2017 nghiatran. All rights reserved.
//

import Foundation

class ArticleRoute: Router {
    
    typealias Element = ArticleViewController
    
    var routerType: RouterType {
        return .root
    }
    
    var viewController: Element {
        let vc = ArticleViewController.xibView()
        return vc
    }
}
