//
//  ApplicationManager.swift
//  Github-MVVM-Bound
//
//  Created by Nghia Tran on 3/20/17.
//  Copyright © 2017 nghiatran. All rights reserved.
//

import UIKit

class ApplicationManager {

    //
    // MARK: - Variable
    static let shared = ApplicationManager()
    
    //
    // MARK: - Public
    func didFinishLaunching(withOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        // Setup SDK
        self.initSDK()
        
        return true
    }
    
    fileprivate func initSDK() {
        
        self.setupLogger()
        self.setupFabric()
    }
    
    // MARK:
    // MARK: Handle App calling
    class func application(_ application: UIApplication, openURL url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        // Create param
        var options: [UIApplicationOpenURLOptionsKey: Any] = [UIApplicationOpenURLOptionsKey.annotation: annotation]
        if let sourceApplication = sourceApplication {
            options[UIApplicationOpenURLOptionsKey.sourceApplication] = sourceApplication as AnyObject?
        }
        
        return self._handleApplication(application, openURL: url, options: options)
    }
    
    class func application(_ app: UIApplication, openURL url: URL, options: [UIApplicationOpenURLOptionsKey : Any]) -> Bool {
        return self._handleApplication(app, openURL: url, options: options)
    }
    
    class func _handleApplication(_ app: UIApplication, openURL url: URL, options: [UIApplicationOpenURLOptionsKey : Any]) -> Bool {
        return true
    }
}

//
// MARK: - Logger
extension ApplicationManager {
    
    func setupLogger() {
        
    }
}

//
// MARK: - Fabric
extension ApplicationManager {
    func setupFabric() {
        
    }
}
