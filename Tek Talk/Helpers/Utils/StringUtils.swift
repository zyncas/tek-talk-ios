//
//  StringUtils.swift
//  YoSing-iOS
//
//  Created by Bao Phan on 5/4/16.
//  Copyright © 2016 Zyncas. All rights reserved.
//

import UIKit

class StringUtils {
    
    class func downloadIMGIXFullImage(url: String, w: String, q: String) -> String {
        let newURL = url + "?w=\(w)&crop=faces%2Ctop&fit=crop&q=50&dpr=2&fm=jpg"
        return newURL
    }
    
    class func downloadIMGIXFullImageWithHeight(url: String, w: String, h: String, q: String) -> String {
        let newURL = url + "?w=\(w)&h=\(h)&crop=faces%2Ctop&fit=crop&q=50&dpr=2&fm=jpg"
        return newURL
    }
    
    class func downloadIMGIXFullScreenImage(url: String, q: String) -> String {
        let newURL = url + "?w=\(Int(UIScreen.main.bounds.size.width))&h=\(Int(UIScreen.main.bounds.size.height))&crop=faces%2Ctop&fit=crop&q=50&dpr=2&fm=jpg"
        return newURL
    }
}
