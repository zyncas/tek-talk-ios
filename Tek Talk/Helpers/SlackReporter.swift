//
//  SlackReporter.swift
//  feels
//
//  Created by Nghia Tran Vinh on 6/23/16.
//  Copyright © 2016 fe. All rights reserved.
//

import Alamofire

enum SlackReporterDataType {
    case error
    case response
}

struct SlackReporterData {

    let type: SlackReporterDataType

    // Slack
    fileprivate lazy var usernameSlack: String = {
        switch self.type {
        case .error:
            return "Susu"
        case .response:
            return "Rolex"
        }
    }()

    fileprivate lazy var icon_emoji: String = {
        switch self.type {
        case .error:
            return ":dog:"
        case .response:
            return ":stopwatch:"
        }
    }()

    // Data
    fileprivate var error: NSError?
    fileprivate var responseTime: CGFloat? = 0
    fileprivate var apiName: String = ""
    fileprivate var fileName: String = ""
    fileprivate var functionName: String = ""
    fileprivate var line: String = ""
    fileprivate var additionInfo: String = ""
    fileprivate lazy var buildNumber: String? = {
        guard let appInfo = Bundle.main.infoDictionary else {return nil}
        let appVersion = appInfo[kCFBundleVersionKey as String] as? String
        return appVersion
    }()

    init(error: NSError?, fileName: String, functionName: String, line: Int) {
        self.type = .error
        self.error = error
        self.functionName = functionName
        self.line = String(line)

        // Filename
        let componment = fileName.components(separatedBy: "/")
        if let _fileName = componment.last {
            self.fileName = _fileName
        } else {
            self.fileName = "unknow"
        }
    }

    init(responseTime: CGFloat?, apiName: String, response: Alamofire.DataResponse<Any>?) {
        self.type = .response
        self.responseTime = responseTime ?? 0
        self.apiName = apiName
        self.additionInfo = self.infoTextFromResponse(response)
    }

    mutating func toParam() -> [String: String] {
        let text = self.toLog()
        let username = self.usernameSlack
        let icon = self.icon_emoji

        let param: [String: String] = ["username": username,
                                       "icon_emoji": icon,
                                       "text": text]
        return param
    }

    fileprivate func removeHTTPSLog(_ text: String) -> String {
        var str = (text as NSString).replacingOccurrences(of: "https://", with: "")
        str = str.replacingOccurrences(of: ".", with: "_")
        return str as String
    }

    fileprivate func infoTextFromResponse(_ response: Alamofire.DataResponse<Any>?) -> String {
        guard let response = response else {return ""}

        var text: String = ""
        if let URL = response.request?.url?.absoluteString {
            let str = self.removeHTTPSLog(URL)
            text += " *URL* = \(str)"
        }

        if let UUID = response.request?.allHTTPHeaderFields?[Constants.APIKey.X_Request_ID] {
            text += " *UUID* = \(UUID)"
        }

        return text
    }

    fileprivate mutating func toLog() -> String {

        // Current User first
        var text: String = ""
        if let currentUser = UserObj.currentUser {
            text += ":dark_sunglasses: \(currentUser.objectId)"
            if let username = currentUser.username {
                text += "|\(username)"
            }
        }

        // Build version
        if let buildVersion = self.buildNumber {
            text += " :iphone: \(buildVersion)"
        }

        // Info
        switch self.type {
        case .error:
            text += ":round_pushpin:\(fileName):\(line) :mag_right:\(functionName)"
            if let error = self.error {
                let str = self.removeHTTPSLog(error.localizedDescription)
                text += " 👉 \(str)"
            } else {
                text += " 👉 Unknow"
            }
            return text
        case .response:
            text += ":round_pushpin:\(self.apiName):"
            if let responseTime = self.responseTime {
                text += " 👉 \(responseTime)"
            } else {
                text += " 👉 Unknow"
            }
            text += " :rocket: \(self.additionInfo)"

            return text
        }

    }
}

class SlackReporter: NSObject {

    static let shareInstance = SlackReporter()

    // MARK: 
    // MARK: Variable
    fileprivate let Token = Constants.Slack.Token
    fileprivate let ErrorChannel = Constants.Slack.ErrorChannel
    fileprivate let ResponseChannel = Constants.Slack.ResponseChannel
    fileprivate lazy var URLErrorChannel: String = {
        //return "https://feelsapp.slack.com/services/hooks/slackbot?token=\(self.Token)&channel=\(self.ErrorChannel)"
        return Constants.Slack.ErrorChannel_Webhook
    }()
    fileprivate lazy var URLResponseChannel: String = {
        //return "https://feelsapp.slack.com/services/hooks/slackbot?token=\(self.Token)&channel=\(self.ResponseChannel)"
        return Constants.Slack.ResponseChannel_Webhook
    }()

    // MARK: 
    // MARK: Public
    func reportErrorData(_ data: SlackReporterData) {

        // Build param
        var data = data
        let param = data.toParam()

        Alamofire.request(self.URLErrorChannel, method: .post, parameters: param, encoding: JSONEncoding.default).responseJSON { (_) in

        }
    }

    func reportResponseData(_ data: SlackReporterData) {

        // Build param
        var data = data
        let param = data.toParam()

        Alamofire.request(self.self.URLResponseChannel, method: .post, parameters: param, encoding: JSONEncoding.default).responseJSON { (_) in

        }
    }

}

extension SlackReporter {
    // Test
    func testSlackReport() {
        let error = NSError.errorWithMessage("Hi, I'm from Error Report")
        let data = SlackReporterData(error: error, fileName: #file, functionName: #function, line:#line)
        self.reportErrorData(data)
    }

    // Test
    func testSlackResponseReport() {
        let data = SlackReporterData(responseTime: 0.2, apiName: "TestAPIName", response: nil)
        self.reportResponseData(data)
    }
}
