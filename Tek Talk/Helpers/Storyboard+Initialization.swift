//
//  Storyboard+Initialization.swift
//  Github-MVVM-Bound
//
//  Created by Nghia Tran on 3/20/17.
//  Copyright © 2017 nghiatran. All rights reserved.
//

import Foundation
import UIKit

protocol StoryboardInitialization {
    associatedtype Element: Identifier
    static var storyboardViewController: Element {get}
}

extension StoryboardInitialization where Element: UIViewController {
    
    static var storyboardViewController: Element {
        let storybroad = UIStoryboard(name: Element.identifier, bundle: nil)
        return storybroad.instantiateViewController(withIdentifier: Element.identifier) as! Element
    }
}
