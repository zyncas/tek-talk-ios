//
//  ColorExtension.swift
//  YoSing-iOS
//
//  Created by Hiep Nguyen on 4/14/16.
//  Copyright © 2016 Zyncas. All rights reserved.
//

import UIKit

extension UIColor {
    
    /** Get color with hex */
    static func color(hex: Int, alpha: CGFloat = 1.0) -> UIColor {
        let red     = CGFloat((hex & 0xFF0000) >> 16) / 255.0
        let green   = CGFloat((hex & 0xFF00) >> 8) / 255.0
        let blue    = CGFloat((hex & 0xFF)) / 255.0
        return UIColor(red: red, green: green, blue: blue, alpha: alpha)
    }
}