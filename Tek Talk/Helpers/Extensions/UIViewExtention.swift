//
//  UIViewExtention.swift
//  Yosing
//
//  Created by Cao Phuoc Thanh on 7/28/16.
//  Copyright © 2016 Zyncas. All rights reserved.
//

import UIKit

extension UIView {
    
    func rotate360(duration: CFTimeInterval, repeatCount: Float) {
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotateAnimation.fromValue = 0.0
        rotateAnimation.toValue = CGFloat((360 * M_PI) / 180)
        rotateAnimation.duration = duration
        
        if repeatCount == 0 {
            rotateAnimation.repeatCount = MAXFLOAT
        } else {
            rotateAnimation.repeatCount = repeatCount
        }
        self.layer.add(rotateAnimation, forKey: "360")
    }
    
    func pop(force: CGFloat = 0.1) {
        let animation = CAKeyframeAnimation(keyPath: "transform.scale")
        animation.values = [0, 0.1*force, -0.1*force, 0.1*force, 0]
        animation.keyTimes = [0, 0.2, 0.4, 0.6, 0.8, 1]
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        animation.duration = 0.7
        animation.isAdditive = true
        animation.repeatCount = 1
        animation.isRemovedOnCompletion = true
        self.layer.add(animation, forKey: "pop")
    }
    
    func shake() {
        let animation = CAKeyframeAnimation()
        animation.keyPath = "position.x"
        animation.values = [0, 10, -10, 10, -5, 5, -5, 0 ]
        animation.keyTimes = [0, 0.125, 0.25, 0.375, 0.5, 0.625, 0.75, 0.875, 1]
        animation.duration = 0.6
        animation.isAdditive = true
        self.layer.add(animation, forKey: "shake")
    }
    /**
     * UIView+setCorner
     */
    public func setCornerWith(radius: CGFloat) {
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
    }
}

extension UIView {
    
    func startDuangAnimation() {
        let op : UIViewAnimationOptions = [.curveEaseInOut, .allowAnimatedContent, .beginFromCurrentState]
        UIView.animate(withDuration: 0.15, delay: 0, options: op, animations: {() -> Void in
            self.layer.setValue((0.80), forKeyPath: "transform.scale")
            }, completion: {(finished: Bool) -> Void in
                UIView.animate(withDuration: 0.15, delay: 0, options: op, animations: {() -> Void in
                    self.layer.setValue((1.3), forKeyPath: "transform.scale")
                    }, completion: {(finished: Bool) -> Void in
                        UIView.animate(withDuration: 0.15, delay: 0, options: op, animations: {() -> Void in
                            self.layer.setValue((1), forKeyPath: "transform.scale")
                            }, completion: { _ in })
                })
        })
    }
    
    func startTransitionAnimation() {
        let transition = CATransition()
        transition.duration = 1.0
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionFade
        self.layer.add(transition, forKey: nil)
    }
}

extension UIView {
    func makeBlurView(targetView:UIView?)
    {
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = targetView!.bounds
        
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight] // for supporting device rotation
        targetView?.addSubview(blurEffectView)
    }
}
