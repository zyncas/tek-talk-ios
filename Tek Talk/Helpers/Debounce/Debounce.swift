//
//  Debounce.swift
//  YoSing-iOS
//
//  Created by Thanh on 5/6/16.
//  Copyright © 2016 Zyncas. All rights reserved.
//

import UIKit

private class Target {
    
    weak var target: AnyObject?
    var action: Selector!
    
    init(target: AnyObject?, action: Selector!) {
        self.target = target
        self.action = action
    }
}

class Debounce {
    
    // MARK: - Public properties / methods
    
    var delayTime: Double = 0.7
    
    func addTarget(target: AnyObject?, action: Selector) {
        let target = Target(target: target, action: action)
        self.targets.append(target)
    }
    
    func process() {
        self.actionCount += 1
        
        self.delay(delayTime: self.delayTime, handler: { () -> Void in
            self.actionCount -= 1
            if self.actionCount == 0 {
                self.triggerEvent()
            }
        })
    }
    
    // MARK: - Private properties / methods
    
    private var targets: [Target] = []
    private var actionCount: Int = 0
    
    private func triggerEvent() {
        for target in targets {
            if let object: AnyObject = target.target {
                Thread.detachNewThreadSelector(target.action, toTarget: object, with: nil)
            }
        }
    }
    
    private func delay(delayTime: Double, handler: @escaping (()->Void)) {
        let delayTimeDispatch = DispatchTime.now()
        DispatchQueue.main.asyncAfter(deadline: delayTimeDispatch) { 
            handler()
        }
    }
}
