//
//  Logger.swift
//  feels
//
//  Created by Nghia Tran Vinh on 5/25/16.
//  Copyright © 2016 fe. All rights reserved.
//

import SwiftyBeaver

// MARK: 
// MARK: Log Instace
class Log: NSObject {

    let log = SwiftyBeaver.self

    // Share instance
    static let shareInstance = Log()

    override init() {
        super.init()

        // Log console
        let console = ConsoleDestination()  // log to Xcode Console
        console.asynchronously = true
        self.log.addDestination(console)
    }

    // MARK: 
    // MARK: Public
    func error(_ error:Any, fileName: String, functionName: String, line: Int) {
        self.log.error(error, fileName, functionName, line: line)
    }

    func warning(_ warning:Any, fileName: String, functionName: String, line: Int) {
        #if DEBUG
            self.log.warning(warning, fileName, functionName, line: line)
        #endif
    }

    func debug(_ debug:Any, fileName: String, functionName: String, line: Int) {
        #if DEBUG
            self.log.debug(debug, fileName, functionName, line: line)
        #endif
    }

    func info(_ info:Any, fileName: String, functionName: String, line: Int) {
        #if DEBUG
            self.log.info(info, fileName, functionName, line: line)
        #endif
    }

    func verbose(_ verbose:Any, fileName: String, functionName: String, line: Int) {
        #if DEBUG
            self.log.verbose(verbose, fileName, functionName, line: line)
        #endif
    }
}

// MARK: 
// MARK: Helper
class Logger: NSObject {
    // Helper
    // MARK: Public
    class func error(_ error:Any, toSlack: Bool = true, fileName: String = #file, functionName: String = #function, line: Int = #line) {

        // Console
        Log.shareInstance.error(error, fileName: fileName, functionName: functionName, line: line)

        if toSlack {
            let errorObj = NSError.errorWithMessage("\(error)")
            SlackReporter.shareInstance.reportErrorData(SlackReporterData(error: errorObj, fileName: fileName, functionName: functionName, line: line))
        }
    }

    class func warning(_ warning:Any, fileName: String = #file, functionName: String = #function, line: Int = #line) {
        Log.shareInstance.warning(warning, fileName: fileName, functionName: functionName, line: line)
    }

    class func debug(_ debug:Any, fileName: String = #file, functionName: String = #function, line: Int = #line) {
        Log.shareInstance.debug(debug, fileName: fileName, functionName: functionName, line: line)
    }

    class func info(_ info:Any, fileName: String = #file, functionName: String = #function, line: Int = #line) {
        Log.shareInstance.info(info, fileName: fileName, functionName: functionName, line: line)
    }

    class func verbose(_ verbose:Any, fileName: String = #file, functionName: String = #function, line: Int = #line) {
        Log.shareInstance.verbose(verbose, fileName: fileName, functionName: functionName, line: line)
    }
}

extension NSError {
    class func errorWithMessage(_ message: String) -> NSError {
        return NSError(domain: "com.fe.feels", code: 999, userInfo: [NSLocalizedDescriptionKey: message])
    }
}
