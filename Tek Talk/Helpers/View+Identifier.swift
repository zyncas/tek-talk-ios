//
//  Identity.swift
//  Github-MVVM-Bound
//
//  Created by Nghia Tran on 3/20/17.
//  Copyright © 2017 nghiatran. All rights reserved.
//

import UIKit

protocol Identifier {
    
    static var identifier: String {get}
}

extension Identifier {
    
    static var identifier: String {
        return String(describing: self)
    }
}

extension UIViewController: Identifier {}
extension UIView: Identifier {}
