//
//  XibInitialization.swift
//  Github-MVVM-Bound
//
//  Created by Nghia Tran on 3/20/17.
//  Copyright © 2017 nghiatran. All rights reserved.
//

import UIKit

protocol XibInitialization {
    
    associatedtype Element: Identifier
    
    static func xib() -> UINib
    
    static func xibView() -> Element
}

extension XibInitialization where Element: UIView {
    
    static func xibView() -> Element {
        let xib = UINib(nibName: Element.identifier, bundle: nil)
        return xib.instantiate(withOwner: self, options: nil).first! as! Element
    }
    
    static func xib() -> UINib {
        return UINib(nibName: Element.identifier, bundle: nil)
    }
}

extension XibInitialization where Element: UIViewController {
    
    static func xibView() -> Element {
        return Element(nibName: Element.identifier, bundle: nil)
    }
    
    static func xib() -> UINib {
        return UINib(nibName: Element.identifier, bundle: nil)
    }
}
