//
//  ArticleViewController.swift
//  Tek Talk
//
//  Created by Doyle Illusion on 1/5/17.
//  Copyright © 2017 Zyncas Technologies. All rights reserved.
//

import Foundation
import UIKit

class ArticleViewController: BaseViewController {
    
    //
    // MARK: - OUTLET
    @IBOutlet weak var tableView: UITableView!
    
    //
    // MARK: - Variable
    fileprivate var dataSource: ArticleDataSource!
    fileprivate var viewModel: ArticleViewModel!
    
    //
    // MARK: - View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initCommon()
        self.initDataSource()
        self.initViewModel()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

//
// MARK: - XibInitialization
extension ArticleViewController: XibInitialization {
    typealias Element = ArticleViewController
}

//
// MARK: - Private
extension ArticleViewController {
    
    func initCommon() {
        
    }
    
    func initDataSource() {
        self.dataSource = ArticleDataSource(with: self.tableView)
        self.dataSource.delegate = self
    }
    
    func initViewModel() {
        self.viewModel = ArticleViewModel()
    }
}

//
// MARK: - Delegate
extension ArticleViewController: CommonDataSourceDelegate {
    func numberOfSection() -> Int {
        return 0
    }
    
    func numberOfRow(in section: Int) -> Int {
        return 0
    }
    
    func object(at indexPath: IndexPath) -> BaseObj {
        return self.viewModel[indexPath.row]
    }
}
