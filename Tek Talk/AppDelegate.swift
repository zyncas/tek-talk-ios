//
//  AppDelegate.swift
//  Tek Talk
//
//  Created by Doyle Illusion on 12/11/16.
//  Copyright © 2016 Zyncas Technologies. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        // Handle
        return ApplicationManager.shared.didFinishLaunching(withOptions: launchOptions)
    }

    func application(_ application: UIApplication, open url: URL,
                     sourceApplication: String?, annotation: Any) -> Bool {
        let handler = ApplicationManager.application(application, openURL: url,
                                                    sourceApplication: sourceApplication, annotation: annotation)
        return handler
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any]) -> Bool {
        let handler = ApplicationManager.application(app, openURL: url, options: options)
        return handler
    }
}

