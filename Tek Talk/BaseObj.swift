//
//  BaseObj.swift
//  Tek Talk
//
//  Created by Nghia Tran on 4/10/17.
//  Copyright © 2017 Zyncas Technologies. All rights reserved.
//

import UIKit
import ObjectMapper

class BaseObj: NSObject, Mappable {

    //
    // MARK: - Variable
    var objectId: Int!
    var createdAt: Date!
    var updatedAt: Date!
    
    //
    // MARK: - Init
    required init?(map: Map) {
        guard map.JSON[Constants.Obj.ObjectId] != nil else {
            print("Can't create obj in BaseModel. Missing ObjectId")
            return nil
        }
    }
    
    override var description: String {
        return Mapper().toJSONString(self, prettyPrint: true)!
    }
    
    func mapping(map: Map) {
        self.objectId <- map[Constants.Obj.ObjectId]
        self.createdAt <- (map[Constants.Obj.CreatedAt], ISO8601DateTransform())
        self.updatedAt <- (map[Constants.Obj.UpdatedAt], ISO8601DateTransform())
    }
    
}
