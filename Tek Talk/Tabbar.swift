//
//  TabbarController.swift
//  Tek Talk
//
//  Created by Doyle Illusion on 1/5/17.
//  Copyright © 2017 Zyncas Technologies. All rights reserved.
//

import UIKit

class Tabbar: UITabBar {
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        self.tintColor = UIColor.color(hex: 0xD0011B)
        self.unselectedItemTintColor = UIColor.color(hex: 0xE9E9E9)
        self.backgroundColor = UIColor.white
        
        for tabBarItem in self.items! {
            tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
        }
    }
}
