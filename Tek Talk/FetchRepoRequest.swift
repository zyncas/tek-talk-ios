//
//  FetchRepoRequest.swift
//  Github-MVVM-Bound
//
//  Created by Nghia Tran on 3/20/17.
//  Copyright © 2017 nghiatran. All rights reserved.
//

import UIKit
import ObjectMapper
import Alamofire

struct FetchItemRequest: Requestable {
    
    var _param: Parameters!
    
    var param: Parameters? {
        return self._param
    }
    
    typealias T = [ItemObj]
    
    var endpoint: String {
        return Constants.APIEndPoint.FeedFetch
    }

    var httpMethod: HTTPMethod {
        return .get
    }
    
    func decode(data: Any) -> Array<ItemObj>? {
        let arrJson = data as! [[String: Any]]
        return Mapper<ItemObj>().mapArray(JSONArray: arrJson)
    }
}
