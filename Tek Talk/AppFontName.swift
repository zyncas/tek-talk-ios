//
//  AppFontName.swift
//  YoSing-iOS
//
//  Created by Bao Phan on 4/25/16.
//  Copyright © 2016 Zyncas. All rights reserved.
//

struct AppFontName {
	static let REGULAR = "SFUIText-Regular"
	static let LIGHT = "SFUIText-Light"
    static let LIGHT_ITALIC	= "SFUIText-LightItalic"
    static let MEDIUM = "SFUIText-Medium"
    static let MEDIUM_ITALIC = "SFUIText-MediumItalic"
    static let BLACK = "SFUIText-Black"
    static let SEMIBOLD = "SFUIText-Semibold"
    static let BOLD = "SFUIText-Bold"
    static let ITALIC = "SFUIText-Italic"
    static let HEAVY = "SFUIText-Heavy"
    
    static let GEORGIA_REGULAR  = "Georgia"
    static let GEORGIA_ITALIC   = "Georgia-Italic"
}
