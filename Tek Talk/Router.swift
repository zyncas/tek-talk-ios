//
//  Route.swift
//  Github-MVVM-Bound
//
//  Created by Nghia Tran on 3/20/17.
//  Copyright © 2017 nghiatran. All rights reserved.
//

import UIKit

enum RouterType {
    case root
    case push
    case present
}

protocol Router {
    
    associatedtype Element: UIViewController
    
    var routerType: RouterType {get}
    var viewController: Element {get}
    
    func handleData(_ block: (Element) -> Void)
}

extension Router {
    
    func handleData(_ block: (Element) -> Void) {
        block(self.viewController)
    }
}
