//
//  UserObj.swift
//  Tek Talk
//
//  Created by Nghia Tran on 4/10/17.
//  Copyright © 2017 Zyncas Technologies. All rights reserved.
//

import UIKit

class UserObj: BaseObj {
    
    //
    // MARK: - Variable
    var username: String?
    
    //
    // MARK: - Current User
    static var currentUser: UserObj?
}
