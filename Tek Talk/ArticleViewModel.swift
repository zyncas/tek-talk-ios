//
//  ArticleViewModel.swift
//  Tek Talk
//
//  Created by Nghia Tran on 4/10/17.
//  Copyright © 2017 Zyncas Technologies. All rights reserved.
//

import Foundation

class ArticleViewModel: BaseViewModel {
    
    //
    // MARK: - Variable
    fileprivate var articleObjs: [ArticleObj] = []
    
    //
    // MARK: - Public
    subscript(index: Int) -> ArticleObj {
        return self.articleObjs[index]
    }
}
