//
//  Constant.swift
//  Tek Talk
//
//  Created by Doyle Illusion on 1/5/17.
//  Copyright © 2017 Zyncas Technologies. All rights reserved.
//

import Foundation

struct Constants {
    
    // APPLICATION
    struct App {
        
        static let isDebugJSON = false
        static let isHTTPS = true
        static let BaseURL: String = {
            if Constants.App.isHTTPS {
                return "https://"
            } else {
                return "http://"
            }
        }()
        
        // Key
        struct Key {
            
        }
        
        // MARK: - Enviroments
        struct Enviroment {
            
            /** API Domain */
            static var API_DOMAIN: String {
                get {
                    #if DEVELOPMENT
                        return "https://yosing.vn"
                    #else
                        return "https://yosing.vn"
                    #endif
                }
            }
            
            static var APP_VERSION: String {
                get {
                    //                #if DEVELOPMENT
                    return "master"
                    //                #else
                    //                    return "v1.1"
                    //                #endif
                }
            }
            
        }
    }
    
    // MARK:
    // MARK: Feels
    struct APIEndPoint {
        static let FeedFetch = ""
    }
    
    // MARK:
    // MARK: KeyAPI
    struct APIKey {
        static let X_Request_ID = "X-Request-ID"
        static let X_Feels_User_Key = "X-Feels-User-Key"
        static let X_Feels_Build = "X-Feels-Build"
        static let X_Feels_Client = "X-Feels-Client"
        static let X_Feels_Secret = "X-Feels-Secret-Key"
        static let X_Feels_Language = "X-Feels-Language"
        static let X_Feels_CountryCode = "X-Feels-Country-Code"
    }
    
    // MARK:
    // MARK: Object
    struct Obj {
        
        // MARK:
        // MARK: Base
        static let CreatedAt = "CreatedAt"
        static let UpdatedAt = "UpdatedAt"
        static let ObjectId = "ID"
        
        // MARK:
        // MARK: USER
        struct User {
            static let Username = "Username"
            static let Email = "Email"
            static let UrlAvatar = "UrlAvatar"
        }
    }
    

    // Slack Report
    struct Slack {
        static let Token = "E4YJuN1mix8r3MbqyqVAjJHG"
        static let ErrorChannel = "%23feels-v3-error"
        static let ResponseChannel = "%23feels-v3-response"
        
        // Webhook integration
        static let ErrorChannel_Webhook = "https://hooks.slack.com/services/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"
        static let ResponseChannel_Webhook = "https://hooks.slack.com/services/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"
    }
}
